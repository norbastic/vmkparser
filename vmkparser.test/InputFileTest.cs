using System.Collections.Generic;
using Xunit;
using vmkparser.lib;
using vmkparser.lib.Models;

namespace vmkparser.test
{
    public class InputFileTest
    {
        [Fact]
        public void ReadTest()
        {
            var input = new InputFile();
            Assert.True(input.Read("C:\\Users\\faragn\\Documents\\vmk\\01plakat4.txt") != null);
        }
    }
}

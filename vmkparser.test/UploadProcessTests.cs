﻿using NuGet.Frameworks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using vmkparser.lib;
using Xunit;

namespace vmkparser.test
{
    public class UploadProcessTests
    {
        [Fact]
        public void XmlGenerateTest()
        {
            var inputFile = new InputFile();
            var items = inputFile.Read("D:\\users\\norba\\Documents\\vmk\\01plakat4.txt");
            var uploadProcess = new UploadProcess();
            var result = uploadProcess.GenerateXml(items[0]);

            Assert.NotNull(result);
        }

        [Fact]
        public void GenerateFiles()
        {
            var inputFile = new InputFile();
            var items = inputFile.Read("D:\\users\\norba\\Documents\\vmk\\01plakat4.txt");

            var rand = new Random();

            foreach (var item in items)
            {
                if (string.IsNullOrWhiteSpace(item.ItemId))
                {
                    continue;
                }

                var filePath = Path.Combine("D:\\temp", $"{item.ItemId}.tif");

                using (var fileStream = new FileStream(filePath, FileMode.CreateNew))
                {
                    fileStream.Seek(100 * 1024 * 1024, SeekOrigin.Begin);
                    fileStream.WriteByte(0);                    
                }      
            }
        }

        [Fact]
        public void ContentsGenerateTest()
        {
            var uploadProcess = new UploadProcess();
            Assert.True(uploadProcess.GenerateContents("D:\\users\\norba\\Documents\\vmk\\images"));

        }

        [Fact]
        public void CopyTest()
        {
            var uploadProcess = new UploadProcess();
            Assert.True(uploadProcess.Copy("Z:\\downloads\\143-74.tif", "D:\\users\\norba\\Documents\\vmk\\test\\image.tiff"));
        }

    }
}

﻿using System;
using System.IO;
using System.Text;
using vmkparser.lib.Models;

namespace vmkparser.lib
{
    public class UploadProcess
    {
        /// <summary>
        /// Creating folder the whole process.
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns>Returns true if folder created.</returns>
        public bool CreateFolder(string folderName)
        {
            try
            {
                Directory.CreateDirectory(folderName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Generating xml with string replacement.
        /// </summary>
        /// <param name="uploadItem"></param>
        /// <returns></returns>
        public string GenerateXml(UploadItem uploadItem)
        {
            const string uri = "http://tlwww.vmk.hu/cgi-bin/tlwww.cgi?show=";
            var run = Directory.GetCurrentDirectory();
            var template = Path.Combine(run, "template.xml");

            if (!File.Exists(template))
            {
                return string.Empty;
            }

            var xml = File.ReadAllText(template);
            var sb = new StringBuilder(xml);
            sb.Replace("$ID$", uploadItem.ItemId);
            sb.Replace("$TITLE$", uploadItem.Title);
            sb.Replace("$URI$", $"{uri}{uploadItem.Id}");
            sb.Replace("$SUBJECTTIME$", uploadItem.SubjectTime);

            return sb.ToString();
        }

        /// <summary>
        /// Generating contents file for Tif files.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool GenerateContents(string path)
        {
            if (!Directory.Exists(path))
            {
                return false;
            }

            var tifs = Directory.GetFiles(path, "*.tif*");
            if (tifs.Length > 0)
            {
                var sb = new StringBuilder();

                for (int i = 0; i < tifs.Length; i++)
                {

                    sb.Append(Path.GetFileName(tifs[i]));
                    sb.Append("\tbundle:PRESERVATION\r\n");
                }

                try
                {
                    using (var writer = new StreamWriter(Path.Combine(path, "contents")))
                    {
                        writer.Write(sb.ToString());
                    }

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Faster copy method.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public bool Copy(string source, string destination)
        {
            int array_length = (int)Math.Pow(2, 19);
            byte[] dataArray = new byte[array_length];
            try
            {
                using (FileStream fsread = new FileStream
                (source, FileMode.Open, FileAccess.Read, FileShare.None, array_length))
                {
                    using (BinaryReader bwread = new BinaryReader(fsread))
                    {
                        using (FileStream fswrite = new FileStream
                        (destination, FileMode.Create, FileAccess.Write, FileShare.None, array_length))
                        {
                            using (BinaryWriter bwwrite = new BinaryWriter(fswrite))
                            {
                                for (; ; )
                                {
                                    int read = bwread.Read(dataArray, 0, array_length);
                                    if (0 == read)
                                        break;
                                    bwwrite.Write(dataArray, 0, read);
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }
    }
}

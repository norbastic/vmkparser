﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using vmkparser.lib.Models;

namespace vmkparser.lib
{
    public class InputFile
    {
        private const string FOCIM = "FOCIM:";
        private const string ALCIM = "ALCIM:";
        private const string TARGYIDO = "TARGYIDO:";
        private const string PELDANY = "PELDANY:";
        private const string INFHORDOZO = "INFHORDOZO:";
        private const string NYOMTATAS = "NYOMTATAS:";
        private const string MERET = "MERET:";
        private const string SZERZ_KOZL = "SZERZ_KOZL:";
        private const string BIBL_MEGJEGYZES = "BIBL_MEGJEGYZES:";
        private const string URL = "URL:";
        private const string MARKANEV = "MARKANEV:";

        public List<UploadItem> Read(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return null;
            }
            
            if (!File.Exists(filePath))
            {
                return null;
            }
            
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            // Encoding is probably Windows
            var srcEncoding = Encoding.GetEncoding("ISO-8859-15");
            var allText = File.ReadAllText(filePath, srcEncoding);
            var listOfItems = new List<UploadItem>();
            
            // Matching for drxxxxxxx.x
            var regex = new Regex("(dr[0-9]+\\.?[0-9]+)");
            var items = regex.Split(allText);

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i].Length == 0)
                {
                    continue;
                }

                // Where drXXXXXXX.X matches and it is not the last item in the array we get the content.
                if (regex.IsMatch(items[i]) && (i != (items.Length - 1)))
                {
                    string focim = GetFocim(items[i + 1]);
                    string targyido = GetTargyido(items[i + 1]);
                    string peldany = GetPeldany(items[i + 1]);

                    var tmp = new UploadItem() { 
                        Id = items[i], 
                        Title = focim, 
                        SubjectTime = targyido, 
                        ItemId = peldany 
                    };

                    listOfItems.Add(tmp);
                }
            }
           
            return listOfItems;
        }

        private string SubStringAndClean(string input, int start, int length)
        {
            string result;

            try
            {
                result = input.Substring(start, length).Replace("\r\n", "").Trim();
                return result;
            }
            catch (Exception)
            {
                return string.Empty;
            }            
        }

        private string GetFocim(string item)
        {
            try
            {
                var focimIndex = item.IndexOf(FOCIM);
                if (focimIndex == -1)
                {
                    return string.Empty;
                }

                string[] focimEnd = {ALCIM, NYOMTATAS, INFHORDOZO, MERET, TARGYIDO, SZERZ_KOZL, BIBL_MEGJEGYZES};

                if (focimEnd.Any(item.Contains))
                {
                    var result = focimEnd.FirstOrDefault(x => item.Contains(x));
                    var startIndex = focimIndex + FOCIM.Length;
                    var endIndex = (item.IndexOf(result) - focimIndex) - FOCIM.Length;

                    return SubStringAndClean(item, startIndex, endIndex);
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return string.Empty;
            }
        }
        
        private string GetTargyido(string item)
        {
            var targyidoIndex = item.IndexOf(TARGYIDO);
            if (targyidoIndex == -1)
            {
                return string.Empty;
            }

            string[] targyidoEnds = { PELDANY, INFHORDOZO, URL };

            if (targyidoEnds.Any(item.Contains))
            {
                var targyIdoEnd = targyidoEnds.First(x => item.Contains(x));
                var start = targyidoIndex + TARGYIDO.Length;
                var end = item.IndexOf(targyIdoEnd) - start;
                var result = SubStringAndClean(item, start, end);

                return result;
            }

            return string.Empty;
        }
        
        private string GetPeldany(string item)
        {

            var peldanyIndex = item.IndexOf(PELDANY);

            if (peldanyIndex == -1)
            {
                return string.Empty;
            }

            string[] peldanyEnds = { MARKANEV, INFHORDOZO };

            if (peldanyEnds.Any(item.Contains))
            {
                var peldanyEnd = peldanyEnds.FirstOrDefault(x => item.Contains(x));
                var peldanyEndIndex = item.IndexOf(peldanyEnd);
                var start = peldanyIndex + PELDANY.Length;
                var end = peldanyEndIndex - start;

                var result = SubStringAndClean(item, start, end).Split(':');

                if (result.Length > 1)
                {
                    return result[1];
                }

                return result[0];
            }

            return string.Empty;
        }
        
    }
}

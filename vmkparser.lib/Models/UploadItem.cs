using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace vmkparser.lib.Models {
    public class UploadItem
    {

        public string Id { get; set; }
        public string Title { get; set; }
        public string SubjectTime { get; set; }
        public string ItemId { get; set; }
        public string ImageLocation { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vmkparser.lib.Models;

namespace vmkparser.ui.Models
{
    public class Settings : ISettings
    {
        public string InputFile { get; set; }
        public string ImageLocation { get; set; }
        public string OutputFolder { get; set; }
        public IEnumerable<UploadItem> UploadItems { get; set; }
    }
}

﻿using System.Collections.Generic;
using vmkparser.lib.Models;

namespace vmkparser.ui.Models
{
    public interface ISettings
    {
        string InputFile { get; set; }
        string ImageLocation { get; set; }
        string OutputFolder { get; set; }
        IEnumerable<UploadItem> UploadItems { get; set; }
    }
}
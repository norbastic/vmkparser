﻿using vmkparser.ui.Views;
using Prism.Ioc;
using Prism.Modularity;
using System.Windows;
using vmkparser.ui.Services;
using vmkparser.ui.Models;

namespace vmkparser.ui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IDataRetrieveService, DataRetrieveService>();
            containerRegistry.RegisterSingleton<ISettings, Settings>();
        }
    }
}

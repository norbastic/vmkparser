﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using vmkparser.lib.Models;
using vmkparser.ui.Models;
using vmkparser.ui.Services;
using Prism.Commands;

namespace vmkparser.ui.ViewModels
{
    public class SecondStepViewModel : BindableBase
    {
        private readonly ISettings _settings;
        private readonly IDataRetrieveService _dataService;
        
        private DelegateCommand checkDataCommand;
        public DelegateCommand CheckDataCommand { get => checkDataCommand = checkDataCommand == null ? new DelegateCommand(CheckData) : checkDataCommand;}

        public ObservableCollection<UploadItem> Data { get; private set; }

        public SecondStepViewModel(ISettings settings, IDataRetrieveService dataRetrieveService)
        {
            _settings = settings;
            _dataService = dataRetrieveService;
            Data = new ObservableCollection<UploadItem>();
        }

        private void CheckData()
        {
            var items = _dataService.Read(_settings.InputFile);
            foreach (var item in items)
            {
                Dispatcher.CurrentDispatcher.InvokeAsync((Action)(() =>
                {
                    Data.Add(item);
                }));

            }
        }
    }
}

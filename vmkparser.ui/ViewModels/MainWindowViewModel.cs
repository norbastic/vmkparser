﻿using ControlzEx.Standard;
using MahApps.Metro.Controls;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections;
using System.Windows.Controls;

namespace vmkparser.ui.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "VMK Parser";
        private DelegateCommand<object> selectionChangedCommand;
        private string bannerText;

        public string BannerText { get => bannerText; set => SetProperty(ref bannerText, value); }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public DelegateCommand<object> SelectionChangedCommand
        {
            get => selectionChangedCommand = selectionChangedCommand == null ?
                new DelegateCommand<object>(SelectionChanged) : selectionChangedCommand;
        }

        public MainWindowViewModel()
        {
        }

        private void SelectionChanged(object sender)
        {
            var args = (SelectionChangedEventArgs)sender;
            var flipView = (FlipView)args.Source;

            switch (flipView.SelectedIndex)
            {
                case 0:
                    BannerText = "1. Adatok begyűjtése";
                    break;
                case 1:
                    BannerText = "2. Adatok ellenőrzése";
                    break;
                default:
                    break;
            }
        }
    }
}

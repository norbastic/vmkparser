﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using vmkparser.gui.Commands;
using Ookii.Dialogs;
using Ookii.Dialogs.Wpf;
using System.Collections.ObjectModel;
using vmkparser.lib.Models;
using vmkparser.lib;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using vmkparser.gui.Views;

namespace vmkparser.gui.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region PrivateTags
        private ICommand _FileOpenCommand;
        private string loadedFileName;
        private ICommand chooseImageLocation;
        private string imageLocation;
        private ICommand chooseOutputFolder;
        private string outputFolder;
        private ICommand loadData;
        private ICommand checkData;
        private ICommand dataLoaded;
        private ICommand close;
        private ICommand startProcessing;
        private bool loadDataEnabled;
        private bool checkDataEnabled;
        private bool _ErrorInfoOpen;
        private ObservableCollection<UploadItem> data;
        private string idError;
        private string titleError;
        private string subjectTimeError;
        private string itemIdError;
        #endregion PrivateTags

        #region PublicTags
        public ObservableCollection<UploadItem> Data
        {
            get => data; // = data == null ? new ObservableCollection<UploadItem>() : data;
            set { 
                data = value; 
                OnPropertyChanged(); 
            }
        }
        public string OutputFolder
        {
            get => outputFolder; set
            {
                outputFolder = value;
                OnPropertyChanged();
            }
        }
        public string ImageLocation
        {
            get => imageLocation;
            set
            {
                imageLocation = value;
                OnPropertyChanged();
            }
        }
        public string LoadedFileName
        {
            get => loadedFileName;
            set
            {
                loadedFileName = value;
                OnPropertyChanged();
            }
        }
        public bool LoadDataEnabled
        {
            get => loadDataEnabled;
            set
            {
                loadDataEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool CheckDataEnabled
        {
            get => checkDataEnabled;
            set
            {
                checkDataEnabled = value; OnPropertyChanged();
            }
        }
        public bool ErrorInfoOpen
        {
            get => _ErrorInfoOpen;
            set
            {
                _ErrorInfoOpen = value;
                OnPropertyChanged();
            }
        }
        public string IdError { get => idError; set { idError = value; OnPropertyChanged(); } }
        public string TitleError { get => titleError; set { titleError = value; OnPropertyChanged(); } }
        public string SubjectTimeError { get => subjectTimeError; set { subjectTimeError = value; OnPropertyChanged(); } }
        public string ItemIdError { get => itemIdError; set { itemIdError = value; OnPropertyChanged(); } }
        #endregion PublicTags

        #region Commands
        public ICommand ChooseOutputFolder
        {
            get => chooseOutputFolder = chooseOutputFolder == null ?
                new RelayCommand(() => OnChooseOutputFolder()) :
                chooseOutputFolder;
        }
        public ICommand ChooseImageLocation
        {
            get => chooseImageLocation = chooseImageLocation == null ?
                new RelayCommand(() => OnChooseImageLocation()) :
                chooseImageLocation;
        }
        public ICommand FileOpenCommand
        {
            get
            {
                if (_FileOpenCommand == null)
                {
                    _FileOpenCommand = new RelayCommand(() => OnFileOpenButton());
                }
                return _FileOpenCommand;
            }
        }
        public ICommand LoadData
        {
            get => loadData = loadData == null ? new RelayCommand(() => OnLoadData()) : loadData;
        }
        public ICommand CheckData
        {
            get => checkData = checkData == null ? new RelayCommand(() => OnCheckData()) : checkData;
        }
        public ICommand DataLoaded
        {
            get => dataLoaded = dataLoaded == null ? new RelayCommand(() => OnDataLoaded()) : dataLoaded;
        }
        public ICommand Close
        {
            get => close = close == null ? new RelayCommand(() => OnDrawerClose()) : close;
        }

        public ICommand StartProcessing
        {
            get => startProcessing = startProcessing == null ? new RelayCommand(() => OnStartProcessing()) : startProcessing;
        }
        #endregion Commands

        #region CommandMethods
        private void OnDataLoaded()
        {            
        }

        private void OnChooseOutputFolder()
        {
            var ooki = new VistaFolderBrowserDialog();
            if (ooki.ShowDialog() == true)
            {
                OutputFolder = ooki.SelectedPath;
            }
        }

        private void OnFileOpenButton()
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                LoadedFileName = dialog.FileName;
                LoadDataEnabled = true;
            }
        }

        private void OnChooseImageLocation()
        {
            var ooki = new VistaFolderBrowserDialog();
            if (ooki.ShowDialog() == true)
            {
                ImageLocation = ooki.SelectedPath;
            }
        }

        private void OnLoadData()
        {
            var inputFile = new InputFile();
            var dataList = inputFile.Read(LoadedFileName);

            foreach (var item in dataList)
            {
                Dispatcher.CurrentDispatcher.BeginInvoke(() => data.Add(item));                
            }

            dataList = null;

            CheckDataEnabled = true;
        }

        private void OnCheckData()
        {
            ErrorInfoOpen = true;
            IdError = data.Where(x => x.Id == "")?.ToList()?.Count().ToString();
            TitleError = data.Where(x => x.Title == "")?.ToList()?.Count().ToString();
            SubjectTimeError = data.Where(x => x.SubjectTime == "")?.ToList()?.Count().ToString();
            ItemIdError = data.Where(x => x.ItemId == "")?.ToList()?.Count().ToString();
        }

        private void OnDrawerClose()
        {
            ErrorInfoOpen = false;
        }
        private void OnStartProcessing()
        {
        }
        #endregion CommandMethods

        #region CTOR
        public MainWindowViewModel()
        {
            data = new ObservableCollection<UploadItem>();
            LoadDataEnabled = false;
            CheckDataEnabled = false;
        }
        #endregion CTOR

        #region boilerplate
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion boilerplate
    }
}

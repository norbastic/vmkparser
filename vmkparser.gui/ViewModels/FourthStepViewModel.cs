﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.Design;
using System.Windows.Threading;
using vmkparser.gui.Services;
using vmkparser.ui.Models;

namespace vmkparser.gui.ViewModels
{
    public class FourthStepViewModel : BindableBase
    {
        private readonly ISettings _settings;
        private readonly IUploadProcessService _uploadProcess;
        private int actualItemIndex = 0;
        private Task task;


        private int _maximum;
        private int _progressValue;
        private DelegateCommand _startProcessCommand;
        private string actualItem;
        private string actualTitle;
        private string actualProcess;
        private string error;
        private bool isButtonEnabled;


        public int Maximum { get => _maximum; set => SetProperty(ref _maximum, value); }
        public int ProgressValue { get => _progressValue; set => SetProperty(ref _progressValue, value); }
        public DelegateCommand StartProcessCommand => _startProcessCommand = _startProcessCommand == null ? new DelegateCommand(StartProcess) : _startProcessCommand;
        public string ActualProcess { get => actualProcess; set => SetProperty(ref actualProcess, value); }
        public string ActualTitle { get => actualTitle; set {

            var newValue = value;

            if (value.Length > 30)
            {
                newValue = $"{value.Substring(0, 30)}...";
            }

            SetProperty(ref actualTitle, newValue); }
        }
        public string ActualItem { get => actualItem; set => SetProperty(ref actualItem, value); }
        public string Error { get => error; set => SetProperty(ref error, value); }
        public bool IsButtonEnabled
        {
            get => isButtonEnabled; 
            set => SetProperty(ref isButtonEnabled, value);
        }


        public FourthStepViewModel(ISettings settings, IUploadProcessService uploadProcessService)
        {
            _settings = settings;
            _uploadProcess = uploadProcessService;
            IsButtonEnabled = true;
            task = new Task(() => Processing());
        }

        private void StartProcess()
        {
            if (_settings.UploadItems == null) return;
            if (_settings.OutputFolder == null) return;

            Maximum = _settings.UploadItems.Count();
            ProgressValue = 0;

            task.Start();
        }

        private void Processing()
        {
            IsButtonEnabled = false;

            foreach (var item in _settings.UploadItems)
            {
                if (item.ItemId == null)
                {
                    continue;
                }

                ActualItem = item.ItemId;
                ActualTitle = item.Title;

                var folder = Path.Combine(_settings.OutputFolder, item.ItemId);
                ActualProcess = "Mappa létrehozása";
                if (!_uploadProcess.CreateFolder(folder))
                {
                    continue;
                }

                // Storing the index of the processed item.
                actualItemIndex++;

                // Updating ProgressBar
                ProgressValue = actualItemIndex;

                ActualProcess = "Tiff fájl másolása";
                if (string.IsNullOrWhiteSpace(item.ImageLocation))
                {
                    continue;
                }
                var sourceImage = Path.GetFileName(item.ImageLocation);
                var destinationImage = Path.Combine(folder, sourceImage);
                _uploadProcess.Copy(item.ImageLocation, destinationImage);

                ActualProcess = "Contents fájl létrehozása";
                _uploadProcess.GenerateContents(folder);

                ActualProcess = "DublinCore létrehozása.";
                var xml = _uploadProcess.GenerateXml(item);

                try
                {
                    File.WriteAllText(Path.Combine(folder, "dublin_core.xml"), xml);
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                }
            }

            IsButtonEnabled = true;
        }
    }
}

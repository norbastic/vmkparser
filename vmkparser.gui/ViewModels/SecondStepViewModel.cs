﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using vmkparser.lib.Models;
using vmkparser.ui.Models;
using vmkparser.ui.Services;
using Prism.Commands;

namespace vmkparser.gui.ViewModels
{
    public class SecondStepViewModel : BindableBase
    {
        private readonly ISettings _settings;
        private readonly IDataRetrieveService _dataService;
        public bool IsCheckDataEnabled { get => isCheckDataEnabled; set => SetProperty(ref isCheckDataEnabled, value); }
        public bool IsLoadDataEnabled { get => isLoadDataEnabled; set => SetProperty(ref isLoadDataEnabled, value); }

        private DelegateCommand loadDataCommand;
        public DelegateCommand LoadDataCommand { get => loadDataCommand = loadDataCommand == null ? new DelegateCommand(LoadData) : loadDataCommand; }

        private DelegateCommand dataCheckedCommand;
        private bool isLoadDataEnabled;
        private bool isCheckDataEnabled;

        public DelegateCommand DataCheckedCommand { get => dataCheckedCommand = dataCheckedCommand == null ? new DelegateCommand(DataChecked) : dataCheckedCommand; }

        public ObservableCollection<UploadItem> Data { get; private set; }
        public SecondStepViewModel(ISettings settings, IDataRetrieveService dataRetrieveService)
        {
            _settings = settings;
            _dataService = dataRetrieveService;
            Data = new ObservableCollection<UploadItem>();
            IsLoadDataEnabled = true;
            IsCheckDataEnabled = false;
        }

        private void LoadData()
        {
            if (_settings.InputFile == null)
            {
                return;
            }

            var items = _dataService.Read(_settings.InputFile);
            foreach (var item in items)
            {
                Dispatcher.CurrentDispatcher.InvokeAsync((Action)(() =>
                {
                    Data.Add(item);
                }));
            }

            IsLoadDataEnabled = false;
            IsCheckDataEnabled = true;
        }

        private void DataChecked()
        {            
            _settings.UploadItems = Data;
            IsCheckDataEnabled = false;
        }
    }
}

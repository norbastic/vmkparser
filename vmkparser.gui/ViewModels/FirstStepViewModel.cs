﻿using Microsoft.Win32;
using Ookii.Dialogs.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vmkparser.ui.Models;

namespace vmkparser.gui.ViewModels
{
    public class FirstStepViewModel : BindableBase
    {
        private string inputFile;
        private string imageLocation;
        private string outputFolder;
        private readonly ISettings _settings;

        public DelegateCommand FileOpenCommand { get; private set; }
        public DelegateCommand ChooseImageLocationCommand { get; private set; }
        public DelegateCommand ChooseOutputFolderCommand { get; private set; }

        public string InputFile { get => inputFile; set => SetProperty(ref inputFile, value); }
        public string ImageLocation { get => imageLocation; set => SetProperty(ref imageLocation, value); }
        public string OutputFolder { get => outputFolder; set => SetProperty(ref outputFolder, value); }

        public FirstStepViewModel(ISettings settings)
        {
            FileOpenCommand = new DelegateCommand(() => FileOpen());
            ChooseImageLocationCommand = new DelegateCommand(() => ChooseImageLocation());
            ChooseOutputFolderCommand = new DelegateCommand(() => ChooseOutputFolder());
            _settings = settings;
        }

        private void FileOpen()
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                InputFile = dialog.FileName;
                _settings.InputFile = InputFile;
            }
        }

        private void ChooseImageLocation()
        {
            var ooki = new VistaFolderBrowserDialog();
            if (ooki.ShowDialog() == true)
            {
                ImageLocation = ooki.SelectedPath;
                _settings.ImageLocation = ImageLocation;
            }
        }

        private void ChooseOutputFolder()
        {
            var ooki = new VistaFolderBrowserDialog();
            if (ooki.ShowDialog() == true)
            {
                OutputFolder = ooki.SelectedPath;
                _settings.OutputFolder = OutputFolder;
            }
        }
    }
}

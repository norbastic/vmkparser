﻿using Prism.Commands;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using vmkparser.lib.Models;
using vmkparser.ui.Models;

namespace vmkparser.gui.ViewModels
{
    public class ThirdStepViewModel : BindableBase
    {
        private DelegateCommand searchImagesCommand;
        private bool isImageCheckEnabled;
        private bool isLocationCheckedEnabled;
        private DelegateCommand imagesCheckedCommand;
        private object itemLock;

        public DelegateCommand SearchImagesCommand =>
            searchImagesCommand =
            searchImagesCommand == null ?
            new DelegateCommand(SearchImages) : searchImagesCommand;

        public DelegateCommand ImagesCheckedCommand =>
            imagesCheckedCommand =
            imagesCheckedCommand == null ?
            new DelegateCommand(ImagesChecked) : imagesCheckedCommand;

        public bool IsImageCheckEnabled { get => isImageCheckEnabled; set => SetProperty(ref isImageCheckEnabled, value); }
        public bool IsLocationCheckedEnabled { get => isLocationCheckedEnabled; set => SetProperty(ref isLocationCheckedEnabled, value); }

        public ObservableCollection<UploadItem> MatchedItems { get; private set; }

        private readonly ISettings _settings;

        public ThirdStepViewModel(ISettings settings)
        {
            _settings = settings;
            itemLock = new object();
            MatchedItems = new ObservableCollection<UploadItem>();
            BindingOperations.EnableCollectionSynchronization(MatchedItems, itemLock);
            IsImageCheckEnabled = true;
            IsLocationCheckedEnabled = false;
        }

        private async void SearchImages()
        {
            await Task.Run(() =>
            {
                if (_settings.UploadItems == null) return;
                if (_settings.ImageLocation == null) return;

                foreach (var uploadItem in _settings.UploadItems)
                {
                    if (string.IsNullOrEmpty(uploadItem.ItemId)) continue;

                    var pattern = $"*{uploadItem.ItemId}*";
                    var files = Directory.GetFiles(_settings.ImageLocation, pattern, SearchOption.AllDirectories);

                    if (files.Length > 0)
                    {
                        uploadItem.ImageLocation = files[0];
                    }
                    else
                    {
                        uploadItem.ImageLocation = "";
                    }

                    lock (itemLock)
                    {
                        MatchedItems.Add(uploadItem);
                    }                    
                }

                IsImageCheckEnabled = false;
                IsLocationCheckedEnabled = true;
            });
        }

        private void ImagesChecked()
        {
            IsLocationCheckedEnabled = false;
            _settings.UploadItems = MatchedItems;
        }
    }
}

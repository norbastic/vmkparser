﻿using vmkparser.lib.Models;

namespace vmkparser.gui.Services
{
    public interface IUploadProcessService
    {
        bool Copy(string source, string destination);
        bool CreateFolder(string folderName);
        bool GenerateContents(string path);
        string GenerateXml(UploadItem uploadItem);
    }
}
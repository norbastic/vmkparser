﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vmkparser.lib.Models;

namespace vmkparser.ui.Services
{
    public interface IDataRetrieveService
    {
        List<UploadItem> Read(string input);
    }
}

﻿using Prism.Ioc;
using vmkparser.gui.Views;
using System.Windows;
using vmkparser.ui.Services;
using vmkparser.ui.Models;
using vmkparser.gui.Services;

namespace vmkparser.gui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IDataRetrieveService, DataRetrieveService>();
            containerRegistry.Register<IUploadProcessService, UploadProcessService>();
            containerRegistry.RegisterSingleton<ISettings, Settings>();            
        }
    }
}
